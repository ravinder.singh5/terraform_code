provider "aws" {
  region  = var.region
}

module "ossec_infra" {
  source                 = "./modules/Ossec_network"
  vpc_cidr_block         = var.vpc_cidr
  vpc_name               = var.vpc_Name
  additional_tags        = var.additional_tags
  dns_hostnames          = var.enable_dns_hostname
  vpc_tenancy            = var.vpc_instance_tenancy
  igw_name               = var.igw_name
  pub_subnet_Name        = var.pub_sn_name
  pub_subnet_CIDR        = var.pub_sn_cidr
  pvt_subnet_Name        = var.pvt_sn_name
  pvt_subnet_CIDR        = var.pvt_sn_cidr
  public_available_zone  = var.public_sn_available_zone
  private_available_zone = var.private_sn_available_zone
  nat_gw_name            = var.NAT_GW_Name
  pub_route_table_name   = var.public_RT_Name
  pvt_route_table_name   = var.private_RT_Name
}

module "ossec_resource" {
  source                = "./modules/resource"
  vpc_id                = module.ossec_infra.vpc_id_output
  pub_SG_ssh_cidr_block = var.ssh_security_group_cidr
  pub_instances_name    = var.pub_instances_name
  pub_subnet_id         = module.ossec_infra.pub_subnet_id_output
  instance_ami          = var.instance_ami
  instance_type         = var.instance_type
  key_name              = var.key_name
  additional_tags       = var.additional_tags
  vpc_cidr_block        = [module.ossec_infra.vpc_cidr_output]
  pvt_instances_name    = var.pvt_instances_name
  pvt_subnet_id         = module.ossec_infra.pvt_subnet_id_output
  alb_sg_id             = module.ossec_loadbalancer.alb_sg_id
  agent_instances_name = var.agent_instances_name
}

module "ossec_loadbalancer" {
  source             = "./modules/load_balancer"
  alb_name           = var.load_balacer_name
  alb_subnets        = module.ossec_infra.pub_subnet_id_output
  vpc_id             = module.ossec_infra.vpc_id_output
  alb_sg_cidr        = var.alb_sg_cidr
  target_instance_id = module.ossec_resource.ossec_server_instanceID_output
  target_group_name  = var.target_group_name
}

