variable "region" {
  type        = string
  description = "provider region"
  default     = "us-east-2"
}


variable "vpc_cidr" {
  type        = string
  description = "vpc cider block"
}

variable "vpc_Name" {
  type = string
}

variable "enable_dns_hostname" {
  type        = bool
  description = "dns hostname"
  default     = true
}

variable "vpc_instance_tenancy" {
  type        = string
  description = "vpc instance tenancy"
  default     = "default"
}

variable "additional_tags" {
  default = {}
  type    = map(any)
}

variable "igw_name" {
  type = string
}

variable "pub_sn_name" {
  type = list(any)
}

variable "pub_sn_cidr" {
  type = list(any)
}

variable "pvt_sn_name" {
  type = list(any)
}

variable "pvt_sn_cidr" {
  type = list(any)
}

variable "public_sn_available_zone" {
  type = list(any)
}

variable "private_sn_available_zone" {
  type = list(any)
}

variable "NAT_GW_Name" {
  type = string
}

variable "public_RT_Name" {
  type = string
}

variable "private_RT_Name" {
  type = string
}

variable "ssh_security_group_cidr" {
  type = list(any)
}

variable "pub_instances_name" {
  type = list(string)
}

variable "instance_ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "key_name" {
  type = string
}

variable "pvt_instances_name" {
  type = list(any)
}

variable "load_balacer_name" {
  type = string
}

variable "alb_sg_cidr" {
  type = list(any)
}

variable "target_group_name" {
  type = string
}

variable "agent_instances_name" {
  type = list(any)
}
