region  = "ap-southeast-1"

########### VPC #############
vpc_cidr             = "10.0.0.0/20"
vpc_Name             = "Ossec_VPC_1"
enable_dns_hostname  = "false"
vpc_instance_tenancy = "default"
additional_tags      = { tool = "ossec", Team = "Ninja", }

#########   igw  #######

igw_name = "Ossec_igw_1"

########  subnets  #######

pub_sn_name = ["Ossec_pub_sn1", "Ossec_pub_sn2"]
pub_sn_cidr = ["10.0.0.0/22", "10.0.4.0/22"]

pvt_sn_name = ["Ossec_pvt_sn1", "Ossec_pvt_sn2", "Ossec_pvt_sn3", "Ossec_pvt_sn4"]
pvt_sn_cidr = ["10.0.8.0/23", "10.0.10.0/23", "10.0.12.0/23", "10.0.14.0/23"]

######## pub_subnet_availability_zones ###########
public_sn_available_zone = ["ap-southeast-1a", "ap-southeast-1b"]

######## pub_subnet_availability_zones ###########
private_sn_available_zone = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c", "ap-southeast-1a"]

############ NAT gateway #####################
NAT_GW_Name = "Ossec_NAT_GW"

############ Route_tables ####################
public_RT_Name  = "Ossec_public_RT"
private_RT_Name = "Ossec_private_RT"

########### Security group ###################
ssh_security_group_cidr = ["183.83.209.185/32"]
########### public instance ################
pub_instances_name = ["Ossec_vpn_server", ]
instance_ami       = "ami-055d15d9cfddf7bd3"
instance_type      = "t2.micro"
key_name           = "ossec"

############ private instances #############

pvt_instances_name = ["Ossec_server"]

############ ossec agent instance ##########
agent_instances_name = ["ossec-agent1"]

############ load balancer ################

load_balacer_name = "Ossec-lb"

alb_sg_cidr = ["183.83.209.185/32"]

target_group_name = "ossec-server-tg"
